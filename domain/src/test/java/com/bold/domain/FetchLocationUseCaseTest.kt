package com.bold.domain

import com.bold.domain.contracts.LocationContract
import com.bold.domain.models.Location
import com.bold.domain.usecases.FetchLocationsUseCase
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class FetchLocationUseCaseTest {

    private val repository = mockk<LocationContract>()

    private val locations = listOf(
        Location(0, "Medellín", "Antioquia", "Colombia"),
        Location(1, "London", "", "")
    )

    private lateinit var useCase: FetchLocationsUseCase

    @Before
    fun setup() {

        coEvery {
            repository.fetchLocations("Medellín")
        } answers {
            locations
        }

        coEvery {
            repository.fetchLocations("erp")
        } answers {
            emptyList()
        }
    }

    @Test
    fun `given location value when invoke function is called then should return location list`() {
        val inputValue = "Medellín"
        useCase = FetchLocationsUseCase(repository)

        val actualLocations = runBlocking {
            useCase.invoke(inputValue)
        }

        coVerify {
            repository.fetchLocations(inputValue)
        }

        Assert.assertEquals(locations, actualLocations)
    }

    @Test
    fun `given location value that does not match when invoke function is called then should return empty list`() {
        val inputValue = "erp"
        useCase = FetchLocationsUseCase(repository)

        val actualLocations = runBlocking {
            useCase.invoke(inputValue)
        }

        coVerify {
            repository.fetchLocations(inputValue)
        }

        Assert.assertEquals(emptyList<Location>(), actualLocations)
    }
}
