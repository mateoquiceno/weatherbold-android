package com.bold.domain.models

data class ForecastDayCondition(
    val date: String,
    val day: Day
)

