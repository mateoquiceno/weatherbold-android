package com.bold.domain.models

data class Location(
    val id: Int,
    val name: String,
    val region: String,
    val country: String
)
