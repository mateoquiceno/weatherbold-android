package com.bold.domain.models

data class Condition(
    val overview: String,
    val icon: String,
    val code: Int
)
