package com.bold.domain.models

data class Forecast(
    val location: Location,
    val currentWeather: CurrentWeather,
    val forecast: ForecastDetail
)