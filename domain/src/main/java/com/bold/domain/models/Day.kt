package com.bold.domain.models

data class Day(
    val averageTemperature: Float,
    val maxTemperature: Float,
    val minTemperature: Float,
    val condition: Condition
)
