package com.bold.domain.models

data class ForecastDetail(
    val days: List<ForecastDayCondition>
)
