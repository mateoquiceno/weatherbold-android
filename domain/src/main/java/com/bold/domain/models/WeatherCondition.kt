package com.bold.domain.models

data class WeatherCondition(
    val text: String,
    val icon: String,
    val code: Int
)
