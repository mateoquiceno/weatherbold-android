package com.bold.domain.models

data class CurrentWeather(
    val temperature: Float,
    val humidity: Float,
    val visibility: Float,
    val uv: Int,
    val feelsLike: Float,
    val condition: WeatherCondition
)
