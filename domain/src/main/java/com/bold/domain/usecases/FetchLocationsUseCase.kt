package com.bold.domain.usecases

import com.bold.domain.contracts.LocationContract
import com.bold.domain.models.Location
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FetchLocationsUseCase @Inject constructor(private val repository: LocationContract) {

    suspend operator fun invoke(inputValue: String): List<Location> {
        return withContext(Dispatchers.IO) {
            repository.fetchLocations(inputValue)
        }
    }
}