package com.bold.domain.usecases

import com.bold.domain.contracts.ForecastContract
import com.bold.domain.models.Forecast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FetchForecastUseCase @Inject constructor(private val repository: ForecastContract) {

    suspend operator fun invoke(location: String): Forecast {
        return withContext(Dispatchers.IO) {
            repository.fetchForecast(location)
        }
    }
}