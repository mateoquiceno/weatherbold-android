package com.bold.domain.contracts

import com.bold.domain.models.Forecast

interface ForecastContract {

    suspend fun fetchForecast(location: String): Forecast
}