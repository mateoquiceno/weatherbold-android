package com.bold.domain.contracts

import com.bold.domain.models.Location

interface LocationContract {

    suspend fun fetchLocations(location: String): List<Location>
}