package com.bold.data.di

import com.bold.data.repository.ForecastRepository
import com.bold.data.repository.LocationRepository
import com.bold.domain.contracts.ForecastContract
import com.bold.domain.contracts.LocationContract
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun bindLocationRepository(locationRepository: LocationRepository): LocationContract

    @Singleton
    @Binds
    abstract fun bindForecastRepository(locationRepository: ForecastRepository): ForecastContract

}