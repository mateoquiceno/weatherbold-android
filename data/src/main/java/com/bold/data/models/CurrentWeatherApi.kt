package com.bold.data.models

import com.bold.domain.models.CurrentWeather
import com.google.gson.annotations.SerializedName

data class CurrentWeatherApi(
    @SerializedName("temp_c")
    val temperature: Float,
    @SerializedName("humidity")
    val humidity: Float,
    @SerializedName("vis_km")
    val visibility: Float,
    @SerializedName("uv")
    val uv: Int,
    @SerializedName("feelslike_c")
    val feelsLike: Float,
    @SerializedName("condition")
    val condition: WeatherConditionApi
) {
    fun toDomain(): CurrentWeather {
        return CurrentWeather(
            temperature = temperature,
            humidity = humidity,
            visibility = visibility,
            uv = uv,
            feelsLike = feelsLike,
            condition = condition.toDomain()
        )
    }
}