package com.bold.data.models

import com.google.gson.annotations.SerializedName
import com.bold.domain.models.Location

data class LocationApi(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("region")
    val region: String,
    @SerializedName("country")
    val country: String
) {

    fun toDomain(): Location {
        return Location(
            id, name, region, country
        )
    }
}
