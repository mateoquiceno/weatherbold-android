package com.bold.data.models

import com.google.gson.annotations.SerializedName
import com.bold.domain.models.CurrentWeather
import com.bold.domain.models.Forecast
import com.bold.domain.models.ForecastDetail

data class ForecastResponseApi(
    @SerializedName("location")
    val locationApi: LocationApi,
    @SerializedName("current")
    val currentWeather: CurrentWeatherApi,
    @SerializedName("forecast")
    val forecast: ForecastDetailApi
) {
    fun toDomain(): Forecast {
        return Forecast(
            location = locationApi.toDomain(),
            currentWeather = currentWeather.toDomain(),
            forecast = forecast.toDomain()
        )
    }
}
