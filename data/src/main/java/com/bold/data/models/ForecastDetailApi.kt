package com.bold.data.models

import com.bold.domain.models.ForecastDetail
import com.google.gson.annotations.SerializedName

data class ForecastDetailApi(
    @SerializedName("forecastday")
    val days: List<ForecastDayConditionApi>
) {
    fun toDomain(): ForecastDetail {
        return ForecastDetail(
            days = days.map {
                it.toDomain()
            }
        )
    }
}
