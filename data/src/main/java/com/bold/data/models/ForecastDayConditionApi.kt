package com.bold.data.models

import com.bold.domain.models.ForecastDayCondition
import com.google.gson.annotations.SerializedName

data class ForecastDayConditionApi(
    @SerializedName("date")
    val date: String,
    @SerializedName("day")
    val day: DayApi
) {
    fun toDomain(): ForecastDayCondition {
        return ForecastDayCondition(
            date = date,
            day = day.toDomain()
        )
    }
}
