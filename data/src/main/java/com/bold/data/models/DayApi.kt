package com.bold.data.models

import com.bold.domain.models.Day
import com.google.gson.annotations.SerializedName

data class DayApi(
    @SerializedName("avgtemp_c")
    val averageTemperature: Float,
    @SerializedName("maxtemp_c")
    val maxTemperature: Float,
    @SerializedName("mintemp_c")
    val minTemperature: Float,
    @SerializedName("condition")
    val condition: ConditionApi
) {
    fun toDomain(): Day {
        return Day(
            averageTemperature = averageTemperature,
            maxTemperature = maxTemperature,
            minTemperature = minTemperature,
            condition = condition.toDomain()
        )
    }
}
