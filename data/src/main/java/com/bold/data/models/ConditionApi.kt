package com.bold.data.models

import com.bold.domain.models.Condition
import com.google.gson.annotations.SerializedName

data class ConditionApi(
    @SerializedName("text")
    val overview: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("code")
    val code: Int
) {
    fun toDomain(): Condition {
        return Condition(
            overview = overview,
            icon = icon,
            code = code
        )
    }
}

