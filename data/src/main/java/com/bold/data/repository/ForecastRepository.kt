package com.bold.data.repository

import com.bold.data.models.ForecastResponseApi
import com.bold.data.network.WeatherService
import com.bold.domain.contracts.ForecastContract
import com.bold.domain.models.Forecast
import javax.inject.Inject

class ForecastRepository @Inject constructor(private val service: WeatherService) :
    ForecastContract {

    private var forecast: Forecast? = null

    override suspend fun fetchForecast(location: String): Forecast {
        val response: ForecastResponseApi? = service.fetchForecast(location).body()
        return response?.let {
            forecast = it.toDomain()
            forecast
        } ?: throw Exception("Location response body not found")
    }
}