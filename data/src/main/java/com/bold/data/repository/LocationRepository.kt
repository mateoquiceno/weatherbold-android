package com.bold.data.repository

import com.bold.data.models.LocationApi
import com.bold.data.network.WeatherService
import com.bold.domain.contracts.LocationContract
import com.bold.domain.models.Location
import retrofit2.Response
import javax.inject.Inject

class LocationRepository @Inject constructor(private val service: WeatherService) :
    LocationContract {

    private var locations: List<Location>? = null

    override suspend fun fetchLocations(location: String): List<Location> {
        val response: Response<List<LocationApi>> = service.fetchLocations(location)
        val locationResponseApi: List<LocationApi>? = response.body()
        return locationResponseApi?.let {
            locations = it.map { locationApi -> locationApi.toDomain() }
            locations
        } ?: throw Exception("Location response body not found")
    }
}