package com.bold.data

import com.bold.data.models.LocationApi
import com.bold.data.network.WeatherService
import com.bold.data.repository.LocationRepository
import com.bold.domain.contracts.LocationContract
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class LocationRepositoryTest {

    private val service = mockk<WeatherService>()
    private val location = "Medellín"
    private val response = mockk<Response<List<LocationApi>>>()

    private lateinit var repository: LocationContract

    @Before
    fun setup() {

        coEvery {
            response.body()
        } answers {
            emptyList()
        }
        coEvery {
            service.fetchLocations(location)
        } answers {
            response
        }
    }

    @Test
    fun `given location value when fetchLocation method is called then should return locations list`() {
        val inputValue = "Medellín"
        repository = LocationRepository(service)

        val actualResponse = runBlocking {
            repository.fetchLocations(inputValue)
        }

        coVerify {
            service.fetchLocations(inputValue)
        }

        Assert.assertEquals(emptyList<LocationApi>(), actualResponse)
    }
}