package com.bold.weatherbold

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeatherBoldApplication : Application()