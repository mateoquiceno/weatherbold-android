package com.bold.weatherbold.mapper

import com.bold.domain.models.Forecast
import com.bold.weatherbold.models.ForecastView

fun Forecast.toForecastView(): ForecastView {
    return ForecastView(Forecast(location, currentWeather, forecast))
}
