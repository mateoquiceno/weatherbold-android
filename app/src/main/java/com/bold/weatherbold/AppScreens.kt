package com.bold.weatherbold

sealed class AppScreen(val route: String) {
    data object WeatherLocationScreen: AppScreen("weather_location_screen")
    data object WeatherLocationDetailScreen: AppScreen("weather_location_detail_screen")
}