package com.bold.weatherbold

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.bold.weatherbold.presentation.ui.DetailWeatherScreen
import com.bold.weatherbold.presentation.ui.WeatherScreen

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = AppScreen.WeatherLocationScreen.route) {
        composable(route = AppScreen.WeatherLocationScreen.route, arguments = listOf(
            navArgument(name = "locationName") {
                type = NavType.StringType
            }
        )) {
            WeatherScreen(navController = navController)
        }
        composable(route = AppScreen.WeatherLocationDetailScreen.route+"/{locationName}") {
            DetailWeatherScreen(navController, it.arguments?.getString("locationName"))
        }
    }
}
