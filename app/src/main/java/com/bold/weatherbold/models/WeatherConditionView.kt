package com.bold.weatherbold.models

import com.bold.domain.models.WeatherCondition

data class WeatherConditionView(
    val text: String,
    val icon: String,
    val code: Int
) {
    constructor(weatherCondition: WeatherCondition) : this(
        weatherCondition.text,
        weatherCondition.icon,
        weatherCondition.code
    )
}
