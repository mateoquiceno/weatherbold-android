package com.bold.weatherbold.models

data class WeatherDetailState(
    val isLoading: Boolean = true,
    val forecastDetailView: ForecastView = ForecastView()
)
