package com.bold.weatherbold.models

import com.bold.domain.models.Day

data class DayView(
    val averageTemperature: Float,
    val maxTemperature: Float,
    val minTemperature: Float,
    val condition: ConditionView
) {
    constructor(day: Day) : this(
        day.averageTemperature,
        day.maxTemperature,
        day.minTemperature,
        ConditionView(
            overview = day.condition.overview,
            icon = day.condition.icon,
            code = day.condition.code
        )
    )
}
