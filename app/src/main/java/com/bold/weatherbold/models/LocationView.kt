package com.bold.weatherbold.models

import com.bold.domain.models.Location

data class LocationView(
    val id: Int,
    val name: String,
    val region: String,
    val country: String
) {
    constructor(location: Location) : this(
        location.id,
        location.name,
        location.region,
        location.country
    )
}
