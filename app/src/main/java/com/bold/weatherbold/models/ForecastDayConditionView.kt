package com.bold.weatherbold.models

import com.bold.domain.models.ForecastDayCondition

data class ForecastDayConditionView(
    val date: String,
    val day: DayView
) {
    constructor(forecastDayCondition: ForecastDayCondition) : this(
        forecastDayCondition.date,
        DayView(forecastDayCondition.day)
    )
}