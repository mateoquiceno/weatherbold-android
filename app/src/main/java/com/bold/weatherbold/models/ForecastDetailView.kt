package com.bold.weatherbold.models

import com.bold.domain.models.ForecastDetail

data class ForecastDetailView(
    val forecastDayConditions: List<ForecastDayConditionView>
) {
    constructor(forecastDetail: ForecastDetail) : this(
        forecastDetail.days.map {
            ForecastDayConditionView(it)

        }
    )
}
