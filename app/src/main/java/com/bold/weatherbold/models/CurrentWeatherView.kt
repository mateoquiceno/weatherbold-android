package com.bold.weatherbold.models

import com.bold.domain.models.CurrentWeather

data class CurrentWeatherView(
    val temperature: Float,
    val humidity: Float,
    val visibility: Float,
    val uv: Int,
    val feelsLike: Float,
    val condition: WeatherConditionView
) {
    constructor(currentWeather: CurrentWeather) : this(
        currentWeather.temperature,
        currentWeather.humidity,
        currentWeather.visibility,
        currentWeather.uv,
        currentWeather.feelsLike,
        WeatherConditionView(currentWeather.condition)
    )
}
