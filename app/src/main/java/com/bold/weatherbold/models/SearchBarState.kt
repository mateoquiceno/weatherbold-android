package com.bold.weatherbold.models

data class SearchBarState(
    val searchText: String = "",
    val isActive: Boolean = false,
    val results: List<LocationView>? = null
)
