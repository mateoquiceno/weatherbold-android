package com.bold.weatherbold.models

import com.bold.domain.models.Forecast

data class ForecastView(
    val location: LocationView? = null,
    val currentWeather: CurrentWeatherView? = null,
    val forecast: ForecastDetailView? = null
) {
    constructor(forecast: Forecast) : this(
        LocationView(forecast.location),
        CurrentWeatherView(forecast.currentWeather),
        ForecastDetailView(forecast.forecast)
    )
}
