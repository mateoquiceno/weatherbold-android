package com.bold.weatherbold.models

import com.bold.domain.models.Condition

data class ConditionView(
    val overview: String,
    val icon: String,
    val code: Int
) {
    constructor(condition: Condition) : this(
        condition.overview,
        condition.icon,
        condition.code
    )
}
