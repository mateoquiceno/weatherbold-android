package com.bold.weatherbold.presentation.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.bold.weatherbold.AppScreen
import com.bold.weatherbold.models.LocationView
import com.bold.weatherbold.presentation.FetchLocationsViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WeatherScreen(navController: NavController) {
    val viewModel: FetchLocationsViewModel = hiltViewModel()

    val searchState by viewModel.searchState.collectAsState()

    Scaffold(
        topBar = {
            SearchBar(
                query = searchState.searchText,
                onQueryChange = viewModel::onSearchTextChange,
                onSearch = { viewModel.onSearch() },
                active = searchState.isActive,
                onActiveChange = {
                    viewModel.onToogleSearch()
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                val locationsView: List<LocationView>? = searchState.results
                locationsView?.let {
                    LazyColumn {
                        if (it.isEmpty()) {
                            item {
                                Text(
                                    text = "Not result found",
                                    modifier = Modifier.padding(
                                        start = 8.dp,
                                        top = 4.dp,
                                        end = 8.dp,
                                        bottom = 4.dp
                                    )
                                )
                            }
                        } else {
                            items(it) { locationView ->
                                VisualContentItemScreen(locationView = locationView) {
                                    navController.navigate(route = AppScreen.WeatherLocationDetailScreen.route + "/${locationView.name}")
                                }
                            }
                        }
                    }
                }
            }
        }
    ) {
        it.calculateBottomPadding()
    }
}

@Composable
private fun VisualContentItemScreen(locationView: LocationView, action: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth()
            .clickable {
                action()
            }
    ) {
        Column {
            Text(
                text = locationView.name,
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(
                    start = 8.dp,
                    top = 4.dp,
                    end = 8.dp,
                    bottom = 4.dp
                )
            )
            Text(
                text = locationView.region + " " + locationView.country,
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.padding(
                    start = 8.dp,
                    top = 4.dp,
                    end = 8.dp,
                    bottom = 4.dp
                )
            )
        }
    }
}
