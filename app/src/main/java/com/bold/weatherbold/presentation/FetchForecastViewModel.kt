package com.bold.weatherbold.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bold.domain.usecases.FetchForecastUseCase
import com.bold.weatherbold.mapper.toForecastView
import com.bold.weatherbold.models.WeatherDetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FetchForecastViewModel @Inject constructor(
    private val fetchForecastUseCase: FetchForecastUseCase
) :
    ViewModel() {

    private val _forecastState: MutableStateFlow<WeatherDetailState> = MutableStateFlow(
        WeatherDetailState()
    )
    val forecastState: StateFlow<WeatherDetailState> = _forecastState

    fun fetchForecast(locationName: String) {
        viewModelScope.launch {
            val forecast = fetchForecastUseCase.invoke(locationName).toForecastView()
            _forecastState.value =
                WeatherDetailState(isLoading = false, forecastDetailView = forecast)
        }
    }
}