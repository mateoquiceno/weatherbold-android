package com.bold.weatherbold.presentation.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.bold.weatherbold.models.ForecastDayConditionView
import com.bold.weatherbold.presentation.FetchForecastViewModel
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailWeatherScreen(navController: NavController, locationName: String?) {
    val viewModel: FetchForecastViewModel = hiltViewModel()
    locationName?.let { viewModel.fetchForecast(it) }
    val forecastState = viewModel.forecastState.collectAsState()
    val forecastView = forecastState.value.forecastDetailView

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Detail " + forecastView.location?.name) },
                navigationIcon = {
                    Icon(
                        modifier = Modifier.clickable { navController.popBackStack() },
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = "back"
                    )
                }
            )
        }
    ) {
        DetailHeaderLoadingScreen(
            isLoading = forecastState.value.isLoading
        ) {
            Column(
                modifier = Modifier
                    .padding(it)
            ) {
                Card(
                    modifier = Modifier
                        .padding(horizontal = 8.dp, vertical = 8.dp)
                        .fillMaxWidth()
                ) {
                    LazyColumn(
                        modifier = Modifier
                            .padding(horizontal = 8.dp, vertical = 8.dp)
                            .fillMaxWidth(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        item {
                            forecastView.location?.name?.let {
                                Text(
                                    text = it,
                                    fontSize = 56.sp
                                )
                            }
                        }
                        item {
                            forecastView.currentWeather?.temperature.let {
                                Text(
                                    text = "$it°",
                                    fontSize = 96.sp
                                )
                            }
                        }
                        item {
                            Row {
                                AsyncImage(
                                    modifier = Modifier
                                        .clip(
                                            shape = RoundedCornerShape(
                                                topStart = 16.dp,
                                                topEnd = 16.dp
                                            )
                                        )
                                        .size(Dp(50f)),
                                    model = "https:${forecastView.currentWeather?.condition?.icon}",
                                    contentDescription = null
                                )
                                forecastView.currentWeather?.condition.let {
                                    it?.text?.let { text ->
                                        Text(
                                            text = text,
                                            fontSize = 32.sp
                                        )
                                    }
                                }
                            }
                        }
                    }
                }

                Card(
                    modifier = Modifier
                        .padding(horizontal = 8.dp, vertical = 8.dp)
                        .fillMaxWidth()
                ) {
                    forecastView.forecast?.forecastDayConditions.let {
                        LazyColumn {
                            if (it.isNullOrEmpty()) {
                                item {
                                    EmptyView()
                                }
                            } else {
                                items(it) { forecastView ->
                                    VisualContentItemScreen(forecastView)
                                }
                            }
                        }
                    }
                }

                LazyVerticalGrid(
                    columns = GridCells.Fixed(2),
                    verticalArrangement = Arrangement.Center,
                    horizontalArrangement = Arrangement.Center
                ) {
                    forecastView.currentWeather?.feelsLike.let {
                        item {
                            it?.let { itFloat ->
                                VisualContentItemCouple(
                                    "$itFloat°",
                                    "Thermal sensation"
                                )
                            }
                        }
                    }
                    forecastView.currentWeather?.humidity.let {
                        item {
                            it?.let { itFloat ->
                                VisualContentItemCouple(
                                    itFloat.toString(),
                                    "Humidity"
                                )
                            }
                        }
                    }
                    forecastView.currentWeather?.visibility.let {
                        item {
                            it?.let { itFloat ->
                                VisualContentItemCouple(
                                    "$itFloat Km",
                                    "Visibility"
                                )
                            }
                        }
                    }
                    forecastView.currentWeather?.uv.let {
                        item {
                            it?.let { itItem ->
                                VisualContentItemCouple(
                                    itItem.toString() + " " + validateTextUv(itItem),
                                    "UV Index"
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun EmptyView() {
    Text(
        text = "Not result found",
        modifier = Modifier.padding(
            start = 8.dp,
            top = 4.dp,
            end = 8.dp,
            bottom = 4.dp
        )
    )
}

@Composable
private fun VisualContentItemScreen(forecastDayConditionView: ForecastDayConditionView) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = convertDateToDayOfWeek(forecastDayConditionView.date),
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier.padding(
                    start = 8.dp,
                    top = 4.dp,
                    end = 8.dp,
                    bottom = 4.dp
                )
            )
            AsyncImage(
                modifier = Modifier
                    .clip(
                        shape = RoundedCornerShape(
                            topStart = 16.dp,
                            topEnd = 16.dp
                        )
                    )
                    .size(Dp(50f)),
                model = "https:${forecastDayConditionView.day.condition.icon}",
                contentDescription = null
            )
            Spacer(Modifier.weight(1f))
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Min Temp.",
                    style = MaterialTheme.typography.bodyLarge,
                    modifier = Modifier.padding(
                        start = 8.dp,
                        top = 4.dp,
                        end = 8.dp,
                        bottom = 4.dp
                    )
                )

                Text(
                    text = forecastDayConditionView.day.minTemperature.toString() + "°",
                    style = MaterialTheme.typography.titleMedium,
                    modifier = Modifier.padding(
                        start = 8.dp,
                        top = 4.dp,
                        end = 8.dp,
                        bottom = 4.dp
                    )
                )
            }

            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Max Temp.",
                    style = MaterialTheme.typography.bodyLarge,
                    modifier = Modifier.padding(
                        start = 8.dp,
                        top = 4.dp,
                        end = 8.dp,
                        bottom = 4.dp
                    )
                )

                Text(
                    text = forecastDayConditionView.day.maxTemperature.toString() + "°",
                    style = MaterialTheme.typography.titleMedium,
                    modifier = Modifier.padding(
                        start = 8.dp,
                        top = 4.dp,
                        end = 8.dp,
                        bottom = 4.dp
                    )
                )
            }
            Spacer(Modifier.weight(1f))
        }
        Divider()
    }
}

@Composable
private fun VisualContentItemCouple(value: String, text: String) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier
                .padding(horizontal = 8.dp, vertical = 8.dp)
        ) {
            Text(
                text = text,
                fontSize = 24.sp
            )
            Text(
                text = value,
                fontSize = 32.sp,
            )
        }
    }
}

private fun convertDateToDayOfWeek(dateString: String): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val date = dateFormat.parse(dateString)
    val dayFormat = SimpleDateFormat("EEE", Locale.getDefault())
    return dayFormat.format(date as Date)
}

private fun validateTextUv(uv: Int): String {
    return if (uv <= 2) {
        "Low"
    } else if (uv in 3..5) {
        "Medium"
    } else if (uv in 6..7) {
        "High"
    } else if (uv in 8..10) {
        "Very high"
    } else {
        "Extreme"
    }
}