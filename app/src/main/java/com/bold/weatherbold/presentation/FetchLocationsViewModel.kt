package com.bold.weatherbold.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bold.domain.models.Location
import com.bold.domain.usecases.FetchLocationsUseCase
import com.bold.weatherbold.models.LocationView
import com.bold.weatherbold.models.SearchBarState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FetchLocationsViewModel @Inject constructor(private val fetchLocationsUseCase: FetchLocationsUseCase) :
    ViewModel() {

    private val _searchState = MutableStateFlow(SearchBarState())
    val searchState = _searchState.asStateFlow()

    fun onSearchTextChange(value: String) {
        _searchState.value = _searchState.value.copy(searchText = value)
    }

    fun onSearch() {
        fetchLocation(_searchState.value.searchText)
    }

    fun onToogleSearch() {
        val prevState = _searchState.value
        _searchState.value  = prevState.copy(isActive = !prevState.isActive)
        if (!_searchState.value.isActive) {
            onSearchTextChange(_searchState.value.searchText)
        }
    }

    private fun fetchLocation(inputValue: String) {
        viewModelScope.launch {
            val locations: List<Location> = fetchLocationsUseCase.invoke(inputValue)
            val locationsView: List<LocationView> = locations.map {
                LocationView(it)
            }
            _searchState.value = _searchState.value.copy(results = locationsView)
        }
    }
}